---
title: "Du travail !"
categories: french thoughts
---

"Notre entreprise fait vivre X salariés."

Je considère que cette phrase, qui sous-entendrait que les entreprises aurait un
but philantropique comme une des plus arnaque intellectuelle qui ait jamais
existée !

Dans la suite de cet article, je vais essayer d'expliquer pourquoi je pense cela.

<!--more-->

### Qui de l'entreprise ou des ses salariés fait vivre l'autre ?

Reprenons à la base : qu'est-ce qu'une entreprise ?

C'est une structure juridique (personne morale) qui a un but (faire des profits),
en produisant (des voitures, des livres, des armes) ou en fournissant des
services (nettoyer des bureaux, créer des sites web).

Avec cette définition très simple d'une entreprise, nulle part il n'est question
de faire vivre des salariés, tout simplement car les salariés ne sont qu'un
moyen (ouvriers, ingénieurs, techniciens de surface) pour arriver au but (les
profits).

Selon ce que l'entreprise produit ou fournit, elle a *besoin* d'êtres humains,
ou de machines (robots, ordinateurs) pour arriver à ses fins.

Donc, la réalité est que les salariés (et les robots) font vivre les entreprises,
jamais l'inverse.

L'inverse s'appelle "Fondation Abbée Pierre" ou "Restos du Coeur", et ce ne sont
pas des entreprises mais des associations à but non lucratif, ou des fondations.

### Baisser les taxes pour créer des emplois, est-ce bien sérieux ?

Comme nous l'avons vu précédemment, le but d'une entreprise c'est de produire de
la "valeur", pas d'embaucher des gens.

Donc, si l'entreprise veut faire plus de profits, soit elle augmente sa
production, soit elle augmente ses prix. A aucun moment, les taxes n'ont
d'incidence sur ce choix.

En effet, si pour augmenter sa production, l'entreprise doit embaucher, elle le
fera, pas pour "faire vivre" des gens, mais pour augmenter ses profits.

Si l'entreprise veut augmenter ses prix, elle sera limité par les prix que ses
concurrents proposent.

Dans ce contexte, baisser les taxes sur les entreprises, que ce soit les charges
patronales, les charges salariales ou les impôts n'a aucun effet sur la décision
d'une entreprise d'embaucher ou pas.

Prenons chaque taxe une par une :

- Baisser les charges patronales permet d'augmenter les bénéfices d'une
  entreprise, sans même avoir à emaucher ! Baisser ce type de taxe est donc
  contre-productif du point de vu de l'emploi, car rappelons-le, le but d'une
  entreprise n'est pas d'embaucher mais de faire du profit, donc si l'entreprise
  peut augmenter son profit sans embaucher, tant mieux !
- Baisser les charges salariales permet aux salariés d'avoir un revenu net plus
  élevé, et donc d'augmenter le fameux "pouvoir d'achat"... Mais il ne faut pas
  oublier le but de ces charges : la protection des salariés en cas de maladie
  ou de chômage ! Donc ce qui semble être une hausse du pouvoir d'achat est en
  réalité une baisse de protection sociale et chômage.
- Baisser les impôts sur les sociétés permet aux entreprises de garder une part
  plus importante de ses bénéfices. Ces bénéfices peuvent être alloués de
  plusieurs manières : dividendes aux actionnaires, intéressement aux salariés,
  report à nouveau. Dans ce scénario, pas de répercussions sur l'emploi. En
  effet, une embauche est principalement dictée par l'espoir que la valeur
  générée soit supérieur au coût du salarié, pas au fait que les actionnaires
  ait reçu plus de dividendes l'année précédente.

### Baissons les impôts et les entreprises (ou les riches) reviendront !

**Alléluia !**

Il y a des entreprises qui disent "nous payons nos impôts en France, estimez
vous heureux". (vidéo Lactalis)

L'argument qui veut que baisser les impôts fera "revenir" ou rester les
entreprises en France est un argument purement économique. Or, je crois que les
entreprises n'ont pas qu'un rôle économique et pour seul but toujours plus de
bénéfices (et de dividendes aux actionnaires). Non, les entreprises ont
également un rôle social, comme tous les citoyens ont également un rôle social.
Ce rôle social se matérialise essentiellement par les impôts (sur les revenus,
sur les sociétés, sur les dividendes etc.), les charges sociales (patronales,
salariales).

Qu'une entreprise pense qu'elle paie trop d'impôts ou que la France devrait
s'estimer heureuse qu'elle les paient en France est un raisonnement cynique et
déshumanisé.
